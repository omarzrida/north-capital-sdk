<?php


namespace Tests;


class AccreditationsTest extends NorthCapitalTest
{
    /** @test */
    public function it_accredits_an_account(): void
    {
        $accountId = $this->createAccountAndGetId();

        $response = $this->sdk()->accredit($accountId);

        $this->assertOk($response);
    }

    /** @test */
    public function it_finds_an_accreditation_by_account_id(): void
    {
        $accountId = $this->createAccountAndGetId();
        $this->sdk()->accredit($accountId);

        $response = $this->sdk()->findAccreditationByAccountId($accountId);

        $this->assertOk($response);
    }

    /** @test */
    public function it_updates_an_accreditation(): void
    {
        $accreditationId = $this->createAccreditationAndGetId();

        $response = $this->sdk()->updateAccreditation($accreditationId);

        $this->assertOk($response);
    }
}