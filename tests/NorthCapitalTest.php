<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use RealBlocks\NorthCapital\NorthCapital;

class NorthCapitalTest extends TestCase
{
    protected function sdk()
    {
//        return new NorthCapital('test-key', 'test-secret', false);
        return new NorthCapital('t9c6PguGV3gFZr0', 'pvQ1qLqmNMC5nLjq2Zp9ONa37Z9ARzZABLH', false);
    }

    protected function accountData()
    {
        return [
            'accountRegistration' => 'Omar Rida - Individual',
            'type' => 'Individual',
            'domesticYN' => 'US Resident',
            'streetAddress1' => '43 W23 ST',
            'city' => 'New York',
            'state' => 'New York',
            'zip' => '10017',
            'country' => 'United States',
            'KYCstatus' => 'Auto Approved',
            'AMLstatus' => 'Auto Approved',
            'AMLdate' => null,
            'field1' => 'some-custom-field',
            'AccreditedStatus' => 'Pending',
            'approvalStatus' => 'Pending'
        ];
    }

    protected function partyData()
    {
        return [
            'firstName' => 'John',
            'lastName' => 'Doe',
            'emailAddress' => 'john@example.com',
            'phone' => '+19171231234',
            'dob' => '10-27-1994',
            'type' => 'Individual',
            'domicile' => 'US Resident',
            'primAddress1' => '123 Main St.',
            'primCity' => 'New York',
            'primState' => 'New York',
            'primZip' => '10016',
            'primCountry' => 'United States',
            'KYCstatus' => 'Pending',
            'AMLstatus' => 'Pending',
            'AMLdate' => null,
            'field1' => 'some-special-field',
            'socialSecurityNumber' => '123121234'
        ];
    }

    protected function createAccountAndGetId()
    {
        $response = $this->sdk()->createAccount($this->accountData());
        return $response['accountDetails'][0]['accountId'];
    }

    protected function createPartyAndGetId()
    {
        $response = $this->sdk()->createParty($this->partyData());
        return $response['partyDetails'][1][0]['partyId'];
    }

    protected function createAccreditationAndGetId()
    {
        $accountId = $this->createAccountAndGetId();
        $response = $this->sdk()->accredit($accountId);

        return $response['accreditedDetails'][0]['airequestId'];
    }

    protected function assertOk($response)
    {
        $this->assertEquals('101', $response['statusCode']);
    }

    /** @test */
    public function gets_new_sdk_instance_from_credentials_and_environment(): void
    {
        $this->assertInstanceOf(NorthCapital::class, $this->sdk());
    }
}