<?php


namespace Tests;


class AccountsTest extends NorthCapitalTest
{
    /** @test */
    public function it_creates_accounts(): void
    {
        $response = $this->sdk()->createAccount($this->accountData());

        $this->assertOk($response);
    }

    /** @test */
    public function it_finds_an_account_by_id(): void
    {
        $newAccountId = $this->createAccountAndGetId();

        $response = $this->sdk()->findAccount($newAccountId);

        $this->assertOk($response);
    }
}