<?php


namespace Tests;


class PartiesTest extends NorthCapitalTest
{
    /** @test */
    public function it_creates_parties(): void
    {
         $response = $this->sdk()->createParty($this->partyData());

         $this->assertOk($response);
    }

    /** @test */
    public function it_finds_a_party_by_id(): void
    {
        $newPartyId = $this->createPartyAndGetId();

        $response = $this->sdk()->findParty($newPartyId);

        $this->assertOk($response);
    }

    /** @test */
    public function it_updates_party_data_by_id(): void
    {
        $newPartyId = $this->createPartyAndGetId();

        $response = $this->sdk()->updateParty($newPartyId, ['KYCstatus' => 'Approved']);

        $this->assertOk($response);
    }
}