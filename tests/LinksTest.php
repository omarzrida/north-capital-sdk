<?php


namespace Tests;


class LinksTest extends NorthCapitalTest
{
    /** @test */
    public function it_creates_links(): void
    {
        $accountId = $this->createAccountAndGetId();
        $partyId = $this->createPartyAndGetId();

        $response = $this->sdk()->createLink($accountId, $partyId);

        $this->assertOk($response);
    }
}