<?php


namespace RealBlocks\NorthCapital;


trait ManagesParties
{
    public function createParty(array $payload): array
    {
        return $this->put('createParty', $payload);
    }

    public function findParty($id): array
    {
        return $this->post('getParty', ['partyId' => $id]);
    }

    public function updateParty($id, array $payload): array
    {
        return $this->post('updateParty', array_merge(['partyId' => $id], $payload));
    }
}