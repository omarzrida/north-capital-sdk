<?php


namespace RealBlocks\NorthCapital;


use GuzzleHttp\Client;

class NorthCapital
{
    use MakesHttpRequests;
    use ManagesAccounts;
    use ManagesParties;
    use ManagesLinks;
    use ManagesAccreditations;

    private Client $client;
    private string $url;
    private array $credentials;

    public function __construct($key, $secret, $production = false)
    {
        $this->setUrl($production);
        $this->setClient($key, $secret);
    }

    private function setUrl(bool $production): void
    {
        if ($production === true) {
            $this->url = 'https://api.norcapsecurities.com/tapiv3/index.php/v3/';
        } else {
            $this->url = 'https://api-sandboxdash.norcapsecurities.com/tapiv3/index.php/v3/';
        }
    }

    private function setClient($key, $secret)
    {
        $this->credentials = [
            'clientID' => $key,
            'developerAPIKey' => $secret
        ];

        $this->client = new Client([
            'base_uri' => $this->url,
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
    }
}