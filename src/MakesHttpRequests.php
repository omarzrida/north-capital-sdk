<?php


namespace RealBlocks\NorthCapital;


use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

trait MakesHttpRequests
{
    public function post(string $uri, array $payload): array
    {
        return $this->request('POST', $uri, $payload);
    }

    public function put(string $uri, array $payload): array
    {
        return $this->request('PUT', $uri, $payload);
    }

    public function request(string $method, string $uri, array $payload): array
    {
        $payload = array_merge($payload, $this->credentials);

        /** @var ResponseInterface $response */
        $response = $this->client->request($method, $uri,
            empty($payload) ? [] : [RequestOptions::JSON => $payload]
        );

        if ($response->getStatusCode() !== 200) {
            $this->handleRequestError($response);
        }

        $responseBody = (string) $response->getBody();
        return json_decode($responseBody, true, 512, JSON_THROW_ON_ERROR) ?: $responseBody;
    }

    /**
     * @param ResponseInterface $response
     * @throws \Exception
     */
    private function handleRequestError(ResponseInterface $response)
    {
        throw new \Exception((string) $response->getBody(), $response->getStatusCode());
    }
}