<?php


namespace RealBlocks\NorthCapital;


trait ManagesLinks
{
    public function createLink($accountId, $partyId): array
    {
        return $this->put('createLink', [
            'firstEntryType' => 'Account',
            'firstEntry' => $accountId,
            'relatedEntryType' => 'IndivACParty',
            'relatedEntry' => $partyId,
            'primary_value' => 1,
            'linkType' => 'owner'
        ]);
    }
}