<?php


namespace RealBlocks\NorthCapital;


trait ManagesAccreditations
{
    public function accredit($accountId): array
    {
        return $this->post('requestAiVerification', [
            'accountId' => $accountId,
            'aiMethod' => 'Upload'
        ]);
    }

    public function findAccreditationByAccountId($accountId): array
    {
        return $this->post('getAiRequest', ['accountId' => $accountId]);
    }

    public function updateAccreditation($accreditationId): array
    {
        return $this->post('updateAiRequest', [
            'airequestId' => $accreditationId,
            'aiRequestStatus' => 'New Info Added'
        ]);
    }
}