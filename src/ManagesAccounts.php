<?php


namespace RealBlocks\NorthCapital;


trait ManagesAccounts
{
    public function createAccount(array $payload): array
    {
        return $this->put('createAccount', $payload);
    }

    public function findAccount($id): array
    {
        return $this->post('getAccount', ['accountId' => $id]);
    }
}